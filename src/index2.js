document.addEventListener('DOMContentLoaded', () => {
  for (let i = 500000, l = 0; i > l; --i) {
    const el = document.createElement('div');
    document.body.appendChild(el);
    document.body.removeChild(el);
  }
  performance.mark('script-dom-content-loaded-processed');
});