import Script1 from './script1';
import Script2 from './script2';
import Script3 from './script3';

document.addEventListener('DOMContentLoaded', () => {
  Script1.bootstrap();
  Script2.bootstrap();
  Script3.bootstrap();
});