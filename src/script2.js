export default {
  bootstrap() {
    performance.mark('script2-start');
    for(let i = 100000, l = 0; i>l; --i) {
      const el = document.createElement('div');
      document.body.appendChild(el);
      document.body.removeChild(el);
    }
    performance.mark('script2-end');
  }
}